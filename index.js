let btn1 = document.getElementById('btn1');
let btn2 = document.getElementById('btn2');
let btn3 = document.getElementById('btn3');
let counter = document.getElementById('count');



let count = 0;
counter.innerText = count;
let intervalId;

function startCounter() {
    count = count + 1;
    counter.innerText = count;
}

btn1.addEventListener('click', () => {
    intervalId = setInterval(startCounter, 1000);
});

function stopCounter() {
    clearInterval(intervalId);
}

btn2.addEventListener('click', stopCounter);

function resetCounter() {
    clearInterval(intervalId);
    count = 0;
    counter.innerText = count;
}

btn3.addEventListener('click', resetCounter);